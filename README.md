#Basic CRUD operation with Spring boot

#use demodb sql to create database in localhost

#Api endpoints:

1. localhost:8080/users - GET, POST
2. localhost:8080/users/1 - GET, UPDATE, DELETE
